# hawk-sqlite

NOTE: this project is now archived, as the backend has been merged into Hawk and will be available from its 2.2.0 release.

[![pipeline status](https://gitlab.com/hawklabs/hawk-sqlite/badges/main/pipeline.svg)](https://gitlab.com/hawklabs/hawk-sqlite/-/commits/main)

This is an experimental [SQLite](http://sqlite.org/)-based backend for the [Eclipse Hawk](https://www.eclipse.org/hawk/) model indexing framework.
SQLite is a fast and lightweight embedded relational database written in C: this backend uses the [Xerial JDBC driver](https://github.com/xerial/sqlite-jdbc) to use it from Java.
This backend requires a Java 8 or newer JDK.

## How to use the backend

There are two ways to use up the backend: installing it into your Eclipse IDE, or as a Maven dependency for your plain Java project.

### Eclipse installation

This assumes that you have already installed Eclipse Hawk.

1. Visit the [private package registry](https://gitlab.com/hawklabs/hawk-sqlite/-/packages).
1. Download the latest zipped update site from the `updatesite` package.
1. Install all plugins from the zipped update site into Eclipse (using "Help - Install New Software...").

### Maven dependency

First, add the private Maven repository to your project:

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/26325704/packages/maven</url>
  </repository>
</repositories>
```

Now add this dependency to the project (check for the latest release in the [private package repository](https://gitlab.com/hawklabs/hawk-sqlite/-/packages/)):

```xml
<dependency>
  <groupId>org.hawklabs</groupId>
  <artifactId>sqlite</artifactId>
  <version>2.2.0-SNAPSHOT</version>
</dependency>
```

## Repository structure

* `backend`: [structured environment](http://blog.vogella.com/2015/12/15/pom-less-tycho-builds-for-structured-environments/) that contains the source code for the SQLite backend, as well as the necessary connections to the Eclipse Hawk backend and integration test suites.
* `benchmarks`: Maven-based project used to test out different ways to structure the database before the backend was developed, and benchmark their relative performance.

## How to develop the backend

### Benchmarks

The benchmarks project is a standard Maven project: you should be able to open it directly with [IntelliJ IDEA](https://www.jetbrains.com/idea/), or import it into your Eclipse workspace as a Maven project (using [M2Eclipse](https://www.eclipse.org/m2e/)).

### Backend

The backend itself is developed as a set of Eclipse plugins: you will need a recent version of "Eclipse IDE for Java Developers" or "Eclipse Modeling Tools" from the [official downloads page](https://www.eclipse.org/downloads/).

1. Make sure all submodules are checked out first with `git submodule update --init`.
1. Install the [IvyDE](https://ant.apache.org/ivy/ivyde/download.html) Ivy integration for Eclipse. Restart Eclipse once complete.
1. From Eclipse, use "File - Import - Existing Projects into Workspace" to import all the projects inside `backend` into your workspace.
1. Right-click on the `org.hawklabs.sqlite` project and select "Ivy - Retrieve dependencies".
1. Open the `org.hawklabs.sqlite.targetplatform` project and open its `.target` file.
1. Wait for the target platform to be fully resolved, and then click on "Set as Target Platform" on the top right of the editor.

All projects should compile correctly now.
You can run the `*TestSuite` classes in the `.tests` plugin by right-clicking on them and selecting "Run As > JUnit Plug-in Test".

## How to build from console

### Eclipse Tycho

The backend can be built using [Maven](https://maven.apache.org/) and [Tycho](https://www.eclipse.org/tycho/), following these steps:

```shell
git submodule update --init
cd backend
mvn install
```

This will also run the backend and integration tests from Eclipse Hawk on the SQLite backend.

### Apache Maven

The core of the backend can be built using plain Maven (without Tycho), so it can be used as a regular Java library:

```shell
cd backend/bundles/org.hawklabs.sqlite
mvn -f pom-plain.xml install
```

This will not run any tests, however.
