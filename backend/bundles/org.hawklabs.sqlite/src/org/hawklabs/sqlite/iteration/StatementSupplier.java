package org.hawklabs.sqlite.iteration;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface StatementSupplier {
	PreparedStatement get() throws SQLException;
}