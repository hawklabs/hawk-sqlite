package org.hawklabs.sqlite.iteration;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface ResultSetFunction<T> {
	T apply(ResultSet rs) throws SQLException;
}