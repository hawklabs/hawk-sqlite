package org.hawklabs.sqlite;

import java.sql.SQLException;

import org.eclipse.hawk.core.graph.IGraphTransaction;
import org.hawklabs.sqlite.SQLiteDatabase.SQLiteConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteTransaction implements IGraphTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteTransaction.class);
	private SQLiteConnection conn;

	public SQLiteTransaction(SQLiteDatabase db) {
		this.conn = db.getGraph();
	}

	@Override
	public void success() {
		try {
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void failure() {
		try {
			conn.rollback();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void close() {
		// no-op: we don't want to close the whole connection for this thread
	}

}
