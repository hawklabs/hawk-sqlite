/*******************************************************************************
 * Copyright (c) 2020 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.hawklabs.sqlite.tests;

import java.io.File;

import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.junit.runners.Parameterized.Parameters;

/**
 * Launcher for the integration tests, using the Neo4j backend.
 * 
 * NOTE: these should be run with the integration.tests project as
 * the working directory.
 */
public class SQLiteIntegrationTestSuite extends IntegrationTestSuite {

	private static final String INTEGRATION_TESTS_PATH = "../../eclipse-hawk/core/tests/org.eclipse.hawk.integration.tests";

	@Parameters(name="{1}")
	public static Object[][] params() {
		return new Object[][] {
			{ new File(INTEGRATION_TESTS_PATH), new SQLiteDatabaseFactory() },
		};
	}

}
