/*******************************************************************************
 * Copyright (c) 2020 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.hawklabs.sqlite.tests;

import java.io.File;
import java.util.Collections;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.integration.tests.bpmn.ModelVersioningTest;
import org.junit.runners.Parameterized.Parameters;

/**
 * Not for CI: this is only so we can easily run a single integration test, by
 * changing the class we extend.
 */
public class SQLiteTestDummy extends ModelVersioningTest {

//	@Parameters(name="{0}")
//	public static Iterable<Object[]> params() {
//		return Collections.singletonList( new Object[] {
//			new SQLiteDatabaseFactory()
//		});
//	}

	@Parameters(name = "{1}")
	public static Iterable<Object[]> params() {
		return Collections.singletonList(new Object[] {
			new File("../../../eclipse-hawk/core/tests/org.eclipse.hawk.integration.tests"),
			new SQLiteDatabaseFactory()
		});
	}

//	public SQLiteTestDummy(IGraphDatabaseFactory dbf) {
//		super(dbf);
//	}

	public SQLiteTestDummy(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf);
	}

}
