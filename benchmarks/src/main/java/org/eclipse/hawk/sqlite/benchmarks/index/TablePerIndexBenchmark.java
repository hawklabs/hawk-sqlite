package org.eclipse.hawk.sqlite.benchmarks.index;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;
import org.eclipse.hawk.sqlite.benchmarks.Row;

public class TablePerIndexBenchmark extends AbstractIndexBenchmark {

	public TablePerIndexBenchmark(DataGenerator gen, int rows, int queryIterations) {
		super(gen, rows, queryIterations);
	}

	@Override
	protected BenchmarkQuery findNodesByIndexKey() throws SQLException {
		return new PreparedByIndexBenchmarkQuery(db,
			(idx) -> String.format("SELECT * FROM idx_%s WHERE keyName = ?;", idx));
	}

	@Override
	protected void loadData() throws IOException, SQLException {
		Map<String, PreparedStatement> statementsByIndex = new HashMap<>();

		try {
			for (int i = 0; i < nRows; i++) {
				Row row = generator.row();
				PreparedStatement stmt = statementsByIndex.computeIfAbsent(row.indexName, (key) -> {
					try (Statement createTableStmt = db.createStatement()) {
						createTableStmt.execute(String.format(
								"CREATE TABLE idx_%s ("
										+ "keyName VARCHAR NOT NULL,"
										+ "keyValue BIGINT NOT NULL,"
										+ "nodeId BIGINT NOT NULL"
										+ ");", key));

						createIndexTableIndices(key, createTableStmt);

						return db.prepareStatement(String.format(
								"INSERT INTO idx_%s (keyName, keyValue, nodeId) VALUES (?, ?, ?);", key));
					} catch (SQLException ex) {
						ex.printStackTrace();
					}

					return null;
				});

				if (stmt != null) {
					stmt.setString(1, row.keyName);
					stmt.setLong(2, row.keyValue);
					stmt.setLong(3, row.nodeId);
					stmt.execute();
					assert stmt.getUpdateCount() == 1;
				}
			}
		} finally {
			for (PreparedStatement stmt : statementsByIndex.values()) {
				stmt.close();
			}
		}
	}

	protected void createIndexTableIndices(String idxName, Statement stmt) throws SQLException {
		// no-op in this class, override
	}

	protected void createSchema() throws SQLException {
		// nothing - the schema is created based on data
	}
}
