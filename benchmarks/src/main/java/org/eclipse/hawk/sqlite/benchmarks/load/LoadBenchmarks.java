package org.eclipse.hawk.sqlite.benchmarks.load;

import java.util.Arrays;
import java.util.Random;

import org.eclipse.hawk.sqlite.benchmarks.Benchmark;
import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;

/**
 * Compares several ways to do large loads with a JDBC driver.
 */
public class LoadBenchmarks {

	public static void main(String[] args) throws Exception {
		final int nRows = 100_000;

		final Benchmark[] benchmarks = new Benchmark[] {
			// Far too slow!
			//new ManyPreparedStatementBenchmark(createGenerator(), nRows, true),

			// Easiest to do with Hawk's API, pretty slow though
			new ManyPreparedStatementBenchmark(createGenerator(), nRows, false),

			// Still pretty slow (autocommit is definitely bad)
			//new SinglePreparedStatementBenchmark(createGenerator(), nRows, true),

			// Fastest one that would be reasonably easy to integrate in the backend
			new SinglePreparedStatementBenchmark(createGenerator(), nRows, false),

			// Uses a CLI SQLite3 client with .mode csv + .import - slower than with the prepared statement
			new CopyCSVFileBenchmark(createGenerator(), nRows),
		};

		for (Benchmark bench : benchmarks) {
			final long benchTime = bench.run();
			System.out.println(String.format("Benchmark %s: %d ms for %d rows", bench, benchTime, nRows));
		}
	}

	protected static DataGenerator createGenerator() {
		final Random rnd = new Random(42);
		final DataGenerator gen = DataGenerator.randomChoice(rnd,
			Arrays.asList("a", "b", "c"),
			Arrays.asList("k1", "k2", "k3"),
			0, 100,
			0, 10_000);
		return gen;
	}

}
