package org.eclipse.hawk.sqlite.benchmarks.index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.eclipse.hawk.sqlite.benchmarks.Benchmark;
import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;

public class IndexBenchmarks {

	public static void main(String[] args) throws Exception {
		final int nRows = 200_000;
		final int nIterations = 5_000;

		final Benchmark[] benchmarks = new Benchmark[] {
			new SingleTableNoIndexBenchmark(createGenerator(), nRows, nIterations),
			new SingleTableAllColumnsIndexedBenchmark(createGenerator(), nRows, nIterations),
			new SingleTableIndexKeyIndexedBenchmark(createGenerator(), nRows, nIterations),

			new TablePerIndexBenchmark(createGenerator(), nRows, nIterations),
			new TablePerIndexWithKeyStringIndexBenchmark(createGenerator(), nRows, nIterations),
			new TablePerIndexAllColumnsIndexedBenchmark(createGenerator(), nRows, nIterations),

			new KeysTableBenchmark(createGenerator(), nRows, nIterations),

			/*
			 * This seems to perform better than the single table approach, but it's still
			 * not as good as using one table per index.
			 */
			new StarSchemaBenchmark(createGenerator(), nRows, nIterations),
		};

		for (Benchmark bench : benchmarks) {
			final long benchTime = bench.run();
			System.out.printf("Benchmark %s: ran %d iterations in %d ms over %d rows%n", bench, nIterations, benchTime, nRows);
		}
	}

	protected static DataGenerator createGenerator() {
		final Random rnd = new Random(42);

		final int nUniqueKeys = 5;
		final List<String> keys = new ArrayList<>();
		for (int i = 0; i < nUniqueKeys; i++) {
			keys.add("k" + i);
		}

		return DataGenerator.randomChoice(rnd,
			Arrays.asList("a", "b", "c"),
			keys,
			0, 100,
			0, 10_000);
	}

}
