package org.eclipse.hawk.sqlite.benchmarks.index;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import java.util.function.Function;
import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;
import org.eclipse.hawk.sqlite.benchmarks.IncrementingCounter;
import org.eclipse.hawk.sqlite.benchmarks.Row;

public class KeysTableBenchmark extends AbstractIndexBenchmark {

	private static final String KEY_IDS_TABLE = "hawk_index_keys";

	public KeysTableBenchmark(DataGenerator gen, int rows, int queryIterations) {
		super(gen, rows, queryIterations);
	}

	@Override
	protected BenchmarkQuery findNodesByIndexKey() throws SQLException {
		return new PreparedByIndexBenchmarkQuery(db,
			(idx) -> String.format(
				"SELECT * FROM idx_%s WHERE keyId = (SELECT id FROM %s WHERE key = ? LIMIT 1);",
				idx, KEY_IDS_TABLE));
	}

	@Override
	protected void loadData() throws IOException, SQLException {
		final Map<String, Integer> keyIDs = new HashMap<>();
		final Map<String, PreparedStatement> indexInsertStatements = new HashMap<>();

		try (PreparedStatement insertKeyStmt = db.prepareStatement(String.format("INSERT INTO %s (id, key) VALUES (?, ?);", KEY_IDS_TABLE))) {
			var counter = new Function<String, Integer>() {
				int counter = 0;

				@Override
				public Integer apply(String key) {
					final int id = counter++;
					try {
						insertKeyStmt.setInt(1, id);
						insertKeyStmt.setString(2, key);
						insertKeyStmt.execute();
						assert insertKeyStmt.getUpdateCount() == 1;
					} catch (SQLException ex) {
						ex.printStackTrace();
					}

					return id;
				}
			};

			for (int i = 0; i < nRows; i++) {
				final Row row = generator.row();
				final int keyID = keyIDs.computeIfAbsent(row.keyName, counter);

				PreparedStatement rowInsertStmt = indexInsertStatements.computeIfAbsent(row.indexName, (idxName) -> {
					try {
						createIndexTable(idxName);
						return db.prepareStatement(String.format(
								"INSERT INTO idx_%s (keyid, keyValue, nodeId) VALUES (?, ?, ?);", idxName));
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
					return null;
				});

				rowInsertStmt.setInt(1, keyID);
				rowInsertStmt.setLong(2, row.keyValue);
				rowInsertStmt.setLong(3, row.nodeId);
				rowInsertStmt.execute();
				assert rowInsertStmt.getUpdateCount() == 1;
			}
		}

		for (PreparedStatement stmt : indexInsertStatements.values()) {
			stmt.close();
		}
	}

	protected void createIndexTable(final String idxName) throws SQLException {
		try (Statement stmt = db.createStatement()) {
			stmt.execute(String.format(
				"CREATE TABLE idx_%s ("
				+ "keyid BIGINT NOT NULL,"
				+ "keyValue BIGINT NOT NULL,"
				+ "nodeId BIGINT NOT NULL"
				+ ");", idxName));

			final String sqlCreateIndex = String.format(
				"CREATE INDEX idx_%s_index ON idx_%s (keyid);",
				idxName, idxName
			);
			// CREATE INDEX idx_a_index ON idx_a (keyid);
			stmt.execute(sqlCreateIndex);
		}
	}

	protected void createSchema() throws SQLException {
		try (Statement stmt = db.createStatement()) {
			stmt.execute(String.format(
				"CREATE TABLE %s ("
				+ "id BIGINT,"
				+ "key VARCHAR PRIMARY KEY"
				+ ");", KEY_IDS_TABLE));
		}
	}
}
