package org.eclipse.hawk.sqlite.benchmarks.index;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;
import org.eclipse.hawk.sqlite.benchmarks.Row;

public class SingleTableNoIndexBenchmark extends AbstractIndexBenchmark {

	public SingleTableNoIndexBenchmark(DataGenerator gen, int rows, int queryIterations) {
		super(gen, rows, queryIterations);
	}

	@Override
	protected PreparedBenchmarkQuery findNodesByIndexKey() throws SQLException {
		return new PreparedBenchmarkQuery(db, "SELECT * FROM data WHERE keyName = ? AND indexName = ?;") {
			@Override
			public void run(Row row) throws SQLException {
				query.setString(1, row.keyName);
				query.setString(2, row.indexName);

				try (ResultSet rs = query.executeQuery()) {
					// Loop through all results
					while (rs.next());
				}
			}
		};
	}

	@Override
	protected void loadData() throws SQLException {
		try (PreparedStatement stmt = db
				.prepareStatement("INSERT INTO data (indexName, keyName, keyValue, nodeId) VALUES (?, ?, ?, ?);")) {
			for (int i = 0; i < nRows; i++) {
				Row row = generator.row();
				stmt.setString(1, row.indexName);
				stmt.setString(2, row.keyName);
				stmt.setLong(3, row.keyValue);
				stmt.setLong(4, row.nodeId);

				stmt.execute();
				assert stmt.getUpdateCount() == 1;
			}
		}
		db.commit();
	}

	protected void createSchema() throws SQLException {
		try (Statement stmt = db.createStatement()) {
			stmt.execute("CREATE TABLE data ("
				+ "indexName VARCHAR NOT NULL,"
				+ "keyName VARCHAR NOT NULL,"
				+ "keyValue BIGINT NOT NULL,"
				+ "nodeId BIGINT NOT NULL"
				+ ");");
		}
	}
}
