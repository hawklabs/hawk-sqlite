package org.eclipse.hawk.sqlite.benchmarks.index;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class PreparedBenchmarkQuery implements BenchmarkQuery {
	protected PreparedStatement query;

	public PreparedBenchmarkQuery(Connection conn, String sql) throws SQLException {
		query = conn.prepareStatement(sql);
	}

	@Override
	public void close() throws Exception {
		if (query != null) {
			query.close();
			query = null;
		}
	}
}