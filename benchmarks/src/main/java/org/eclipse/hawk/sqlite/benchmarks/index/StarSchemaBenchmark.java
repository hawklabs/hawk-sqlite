package org.eclipse.hawk.sqlite.benchmarks.index;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;
import org.eclipse.hawk.sqlite.benchmarks.Row;

public class StarSchemaBenchmark extends AbstractIndexBenchmark {

	private static final String KEY_IDS_TABLE = "hawk_keys";
	private static final String INDEX_IDS_TABLE = "hawk_indices";
	private static final String DATA_TABLE = "data";

	public StarSchemaBenchmark(DataGenerator gen, int rows, int queryIterations) {
		super(gen, rows, queryIterations);
	}

	@Override
	protected BenchmarkQuery findNodesByIndexKey() {
		return new PreparedByIndexBenchmarkQuery(db,
			(idx) -> String.format(
				"SELECT * FROM data WHERE keyId = (SELECT id FROM %s WHERE key = ? LIMIT 1) AND indexId = (SELECT id FROM %s WHERE indexName = '%s');",
				KEY_IDS_TABLE, INDEX_IDS_TABLE, idx));
	}

	private static class IDGenerator implements Function<String, Integer> {
		private int counter = 0;
		private final PreparedStatement insertKeyStmt;

		public IDGenerator(PreparedStatement stmt) {
			this.insertKeyStmt = stmt;
		}

		@Override
		public Integer apply(String s) {
			final int id = counter++;
			try {
				insertKeyStmt.setInt(1, id);
				insertKeyStmt.setString(2, s);
				insertKeyStmt.execute();
				assert insertKeyStmt.getUpdateCount() == 1;
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			return id;
		}
	}

	@Override
	protected void loadData() throws SQLException {

		try (var insertKeyStmt = db.prepareStatement(String.format("INSERT INTO %s (id, key) VALUES (?, ?);", KEY_IDS_TABLE));
			 var insertIndexStmt = db.prepareStatement(String.format("INSERT INTO %s (id, indexName) VALUES (?, ?);", INDEX_IDS_TABLE));
			 var insertRowStmt = db.prepareStatement(String.format("INSERT INTO %s (indexId, keyId, keyValue, nodeId) VALUES (?, ?, ?, ?);", DATA_TABLE))) {

			final Map<String, Integer> keyIDs = new HashMap<>();
			final Map<String, Integer> indexIDs = new HashMap<>();

			final var keyIDGenerator = new IDGenerator(insertKeyStmt);
			final var indexIDGenerator = new IDGenerator(insertIndexStmt);

			for (int i = 0; i < nRows; i++) {
				final Row row = generator.row();

				final int keyID = keyIDs.computeIfAbsent(row.keyName, keyIDGenerator);
				final int indexID = indexIDs.computeIfAbsent(row.indexName, indexIDGenerator);
				insertRowStmt.setInt(1, indexID);
				insertRowStmt.setInt(2, keyID);
				insertRowStmt.setLong(3, row.keyValue);
				insertRowStmt.setLong(4, row.nodeId);
				insertRowStmt.execute();
				assert insertRowStmt.getUpdateCount() == 1;
			}
		}

		try (Statement stmt = db.createStatement()) {
			stmt.execute(String.format(
				"CREATE INDEX i_data ON %s (indexId, keyId);",
				DATA_TABLE
			));
		}

		db.commit();
	}

	protected void createSchema() throws SQLException {
		try (Statement stmt = db.createStatement()) {
			stmt.execute(String.format(
				"CREATE TABLE %s ("
				+ "id BIGINT UNIQUE,"
				+ "key VARCHAR PRIMARY KEY"
				+ ");", KEY_IDS_TABLE));

			stmt.execute(String.format(
					"CREATE TABLE %s ("
					+ "id BIGINT UNIQUE,"
					+ "indexName VARCHAR PRIMARY KEY"
					+ ");", INDEX_IDS_TABLE));

			stmt.execute(String.format(
					"CREATE TABLE %s ("
					+ "indexId BIGINT NOT NULL,"
					+ "keyId BIGINT NOT NULL,"
					+ "keyValue BIGINT NOT NULL,"
					+ "nodeId BIGINT NOT NULL"
						+ ");", DATA_TABLE));
		}
	}
}
