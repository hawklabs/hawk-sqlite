package org.eclipse.hawk.sqlite.benchmarks.index;

import java.sql.SQLException;

public interface BenchmarkQuerySupplier {
	BenchmarkQuery get() throws SQLException;
}