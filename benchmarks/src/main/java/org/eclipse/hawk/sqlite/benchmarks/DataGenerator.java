package org.eclipse.hawk.sqlite.benchmarks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

public class DataGenerator {
	private final Supplier<String> indexNameSupplier;
	private final Supplier<String> keyNameSupplier;
	private final Supplier<Long> keyValueSupplier;
	private final Supplier<Long> nodeIdSupplier;

	private static class OutputFile {
		public final File file;
		public final PrintWriter writer;
		public OutputFile(File file, PrintWriter writer) {
			super();
			this.file = file;
			this.writer = writer;
		}
	}

	public DataGenerator(Supplier<String> idxNameSupplier, Supplier<String> keyNameSupplier,
			Supplier<Long> keyValueSupplier, Supplier<Long> nodeIdSupplier) {
		this.indexNameSupplier = idxNameSupplier;
		this.keyNameSupplier = keyNameSupplier;
		this.keyValueSupplier = keyValueSupplier;
		this.nodeIdSupplier = nodeIdSupplier;
	}

	public Row row() {
		return new Row(
			indexNameSupplier.get(),
			keyNameSupplier.get(),
			keyValueSupplier.get(),
			nodeIdSupplier.get()
		);
	}

	public void csv(File fCSV, int nRows) throws IOException {
		try (FileWriter fos = new FileWriter(fCSV, StandardCharsets.UTF_8); PrintWriter pw = new PrintWriter(fos)) {
			// SQLite doesn't need the header row if the table exists
			//pw.println("indexName,keyName,keyValue,nodeId");

			for (int i = 0; i < nRows; i++) {
				Row row = row();
				pw.println(row.indexName + "," + row.keyName + "," + row.keyValue + "," + row.nodeId);
			}
		}
	}

	/**
	 * Creates a CSV for a table with indexID + keyID + keyVal + keyName, and
	 * writes the contents of the associated indexID + keyID files.
	 */
	public File csvStarSchema(int nRows, File fKeyIDs, File fIndexIDs) throws IOException {
		File fStar = File.createTempFile("star", ".csv");
		fStar.deleteOnExit();

		try (PrintWriter pwStar = getWriter(fStar);
			 PrintWriter pwKeys = getWriter(fKeyIDs);
			 PrintWriter pwIndexes = getWriter(fIndexIDs)) {
			
			pwStar.println("indexId|keyId|keyValue|nodeId");
			pwKeys.println("id|key");
			pwIndexes.println("id|index");

			final Map<String, Integer> keyIDs = new HashMap<>();
			final Map<String, Integer> indexIDs = new HashMap<>();
			final Function<String, Integer> keyIDFunction = null;// new StringIdentifierGenerator(pwKeys);
			final Function<String, Integer> indexIDFunction = null; //new StringIdentifierGenerator(pwIndexes);

			for (int i = 0; i < nRows; i++) {
				final Row row = row();
				final int indexID = indexIDs.computeIfAbsent(row.indexName, indexIDFunction);
				final int keyID = keyIDs.computeIfAbsent(row.keyName, keyIDFunction);
				pwStar.println(indexID + "|" + keyID + "|" + row.keyValue + "|" + row.nodeId);
			}
		}

		return fStar;
	}

	protected PrintWriter getWriter(File fKeyIDs) throws IOException {
		return new PrintWriter(new FileWriter(fKeyIDs, StandardCharsets.UTF_8));
	}

	public static DataGenerator randomChoice(Random rnd, List<String> idxNames, List<String> keyNames, long minKeyVal, long maxKeyVal, long minNodeId, long maxNodeId) {
		return new DataGenerator(
			choiceRandom(rnd, idxNames),
			choiceRandom(rnd, keyNames),
			intervalRandom(rnd, minKeyVal, maxKeyVal),
			intervalRandom(rnd, minNodeId, maxNodeId)
		);
	}

	private static <T> Supplier<T> choiceRandom(Random rnd, List<T> l) {
		assert !l.isEmpty();
		return () -> l.get(rnd.nextInt(l.size()));
	}

	private static Supplier<Long> intervalRandom(Random rnd, long min, long max) {
		final long range = max - min + 1;
		return () -> min + (rnd.nextLong() % range);
	}
}