package org.eclipse.hawk.sqlite.benchmarks.load;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.hawk.sqlite.benchmarks.Benchmark;
import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;

public abstract class AbstractLoadBenchmark implements Benchmark {

	protected final DataGenerator generator;
	protected final int nRows;
	protected final boolean autocommit;
	protected Connection db;
	protected File dbFile;

	public AbstractLoadBenchmark(DataGenerator gen, int rows, boolean autocommit) {
		this.generator = gen;
		this.nRows = rows;
		this.autocommit = autocommit;
	}

	@Override
	public long run() throws Exception {
		setupDatabase();
		db.setAutoCommit(autocommit);

		final long millisStart = System.currentTimeMillis();
		loadData();
		final long millisEnd = System.currentTimeMillis();
		checkRowCount();

		db.close();

		return millisEnd - millisStart;
	}

	protected void checkRowCount() throws SQLException {
		try (Statement stmt = db.createStatement()) {
			ResultSet rs = stmt.executeQuery("SELECT COUNT(1) FROM data;");
			if (rs.next()) {
				final int countedRows = rs.getInt(1);
				if (countedRows != nRows) {
					throw new RuntimeException(String.format("Row count for %s is wrong: expected %d, got %d", this, nRows, countedRows));
				}
			} else {
				throw new RuntimeException("No results returned from row count check?");
			}
		}
	}

	protected void setupDatabase() throws IOException, ClassNotFoundException, SQLException {
		dbFile = File.createTempFile("sqlitebench", ".db");
		dbFile.delete();
		dbFile.deleteOnExit();
	
		this.db = DriverManager.getConnection("jdbc:sqlite:" + dbFile.getPath());
	
		try (Statement stmt = db.createStatement()) {
			stmt.execute("CREATE TABLE data ("
				+ "indexName VARCHAR NOT NULL,"
				+ "keyName VARCHAR NOT NULL,"
				+ "keyValue BIGINT NOT NULL,"
				+ "nodeId BIGINT NOT NULL"
				+ ");");
		}
	}

	protected abstract void loadData() throws Exception;

}