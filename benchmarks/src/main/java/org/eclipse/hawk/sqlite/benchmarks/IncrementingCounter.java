package org.eclipse.hawk.sqlite.benchmarks;

import java.io.PrintWriter;
import java.util.function.Function;

public class IncrementingCounter<T> implements Function<T, Integer> {
    int keyIDCounter = 0;

    @Override
    public Integer apply(T key) {
        return keyIDCounter++;
    }
}
