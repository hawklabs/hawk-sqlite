package org.eclipse.hawk.sqlite.benchmarks;

public interface Benchmark {
	long run() throws Exception;
}
