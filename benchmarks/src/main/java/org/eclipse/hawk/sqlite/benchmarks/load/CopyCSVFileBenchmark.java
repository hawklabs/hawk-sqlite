package org.eclipse.hawk.sqlite.benchmarks.load;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.Statement;

import org.eclipse.hawk.sqlite.benchmarks.Benchmark;
import org.eclipse.hawk.sqlite.benchmarks.DataGenerator;

public class CopyCSVFileBenchmark extends AbstractLoadBenchmark implements Benchmark {

	public CopyCSVFileBenchmark(DataGenerator gen, int nRows) {
		super(gen, nRows, false);
	}

	@Override
	protected void loadData() throws Exception {
		File fCSV = File.createTempFile("sqlite", ".csv");
		fCSV.deleteOnExit();
		generator.csv(fCSV, nRows);

		// JDBC does not have support for the CSV import. Need to rely on CLI tool being in the PATH (sqlite3)
		Process sqliteProcess = new ProcessBuilder()
				.command("sqlite3", dbFile.getAbsolutePath())
				.redirectErrorStream(true)
				.directory(fCSV.getParentFile())
				.start();

		try (PrintStream ps = new PrintStream(sqliteProcess.getOutputStream())) {
			ps.println(".mode csv");
			ps.println(String.format(".import %s data", fCSV.getName()));
		}
		try (BufferedReader br = new BufferedReader(new InputStreamReader(sqliteProcess.getInputStream()))) {
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				System.out.println(line);
			}
		}

		int result = sqliteProcess.waitFor();
		if (result != 0) {
			throw new RuntimeException("SQLite import failed with status code " + result);
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}