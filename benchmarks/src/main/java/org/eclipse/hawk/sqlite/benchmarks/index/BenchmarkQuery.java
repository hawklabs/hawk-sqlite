package org.eclipse.hawk.sqlite.benchmarks.index;

import java.sql.SQLException;

import org.eclipse.hawk.sqlite.benchmarks.Row;

public interface BenchmarkQuery extends AutoCloseable {

	void run(Row row) throws SQLException;

}
