package org.eclipse.hawk.sqlite.benchmarks.index;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.hawk.sqlite.benchmarks.Row;

public class PreparedByIndexBenchmarkQuery implements BenchmarkQuery {
	private final Connection conn;
	private final Function<String, String> sqlGenerator;
	private Map<String, PreparedStatement> queries = new HashMap<>();

	public PreparedByIndexBenchmarkQuery(Connection conn, Function<String, String> sqlGeneratorFromIndex) {
		this.conn = conn;
		this.sqlGenerator = sqlGeneratorFromIndex;
	}

	@Override
	public void close() throws Exception {
		if (queries != null) {
			for (PreparedStatement stmt : queries.values()) {
				stmt.close();
			}
			queries = null;
		}
	}

	@Override
	public void run(Row row) throws SQLException {
		PreparedStatement query = queries.computeIfAbsent(row.indexName, (idx) -> {
			try {
				return conn.prepareStatement(sqlGenerator.apply(idx));
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		});

		assert query != null;
		query.setString(1, row.keyName);
		try (ResultSet rs = query.executeQuery()) {
			// Make sure to loop through all results
			while (rs.next());
		}
	}
}